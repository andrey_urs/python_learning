# pip install virtualenv
# virtualenv ENV
# C:\ENV\Scripts\activate
# pip install requests sqlalchemy beautifulsoup4

from bs4 import BeautifulSoup
from request_helpers import save_amazon_page, getFileContent
from product import Product


def _main():
    srs = 7296466011
    page = 9
    folder = 'D:/websites/python_learning/amazon_parser/saved_html/'

    saved_file_name = save_amazon_page(srs,page,folder)
    html_doc = getFileContent(saved_file_name)
    soup = BeautifulSoup(html_doc, 'html.parser')
    html_items = soup.find_all("li", class_="s-result-item")

    for cur_item in html_items:
        try:
            asin = cur_item['data-asin']
        except:
            continue
        name = cur_item.find("h2").string
        pre_seller = cur_item.find("span", class_="a-size-small a-color-secondary", string="by ")
        seller = pre_seller.next_sibling.string if (pre_seller) else ''

        price_integer = cur_item.find("span", class_="sx-price-whole").string
        price_integer = price_integer.replace(",","")
        price_fractional = cur_item.find("sup", class_="sx-price-fractional").string
        price = int(price_integer) + int(price_fractional)/100

        status = Product.save_product(name,asin,price,seller)
        # if status != 'skipped':
        #     print(asin+" : "+str(price))


if __name__ == "__main__":
    _main()


# C:\ENV\Scripts\deactivate