from sqlalchemy import Column, String, Integer
from base import Session, engine, Base
from datetime import datetime


class Product(Base):
    __tablename__ = 'product'

    id=Column(Integer, primary_key=True)
    name=Column(String)
    asin=Column(String)
    price=Column(String)
    seller=Column(String)
    date=Column(String, nullable=False, default=str(datetime.now().strftime(r"%Y-%m-%d %H:%M:%S")))
    iscurrent=Column(Integer, nullable=False, default=1)

    def __init__(self, name, asin, price, seller, \
            date = str(datetime.now().strftime(r"%Y-%m-%d %H:%M:%S")), iscurrent = 1):
        self.name = name
        self.asin = asin
        self.price = price
        self.seller = seller
        self.date = date
        self.iscurrent = iscurrent


    def save_product(name, asin, price, seller):
        Base.metadata.create_all(engine)
        session = Session()

        this_product = Product.get_existing_product(asin)
        current_product = None

        if this_product:
            existing_price = this_product.price

            if float(existing_price) != float(price):
                current_product = Product(name, asin, price, seller)
                this_product.iscurrent = 0
                status = 'updated'
            else:
                status = 'skipped'
        else:
            current_product = Product(name, asin, price, seller)
            status = 'added'

        if current_product != None:
            print(asin+" : "+str(price))
            session.add(current_product)
            session.commit()

        session.close()


    def get_existing_product(asin):
        try:
            session
        except NameError:
            is_new_session = True
            session = Session()
        else:
            is_new_session = False
            
        existing_product = session.query(Product).filter(Product.asin == asin, Product.iscurrent == 1).first()
        if is_new_session:
            session.close()
        return existing_product
