import csv
import datetime

price_max = None
price_min = None
data = {}


with open("D:/websites/python_learning/csv/GDAX-ETH_USD.csv") as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        date_cur = datetime.datetime.strptime(row['Date'], f'%Y-%m-%d')
        month_cur = str(date_cur.year)+'-'+str(date_cur.month)
        price_high = float(row['High'])
        price_low = float(row['Low'])
        volume = float(row['Volume'])
        price_max = max(price_max,price_high) if price_max != None else price_high
        price_min = min(price_min,price_low) if price_min != None else price_low
        data[month_cur] = data[month_cur] + volume if month_cur in data.keys() else volume
    print(price_min)
    print(price_max)
    print(data)

