# pip install virtualenv
# virtualenv ENV
# C:\ENV\Scripts\activate
# pip install requests sqlalchemy beautifulsoup4

from bs4 import BeautifulSoup
from request_helpers import save_amazon_page, getFileContent
from base import Session, engine, Base
from product import Product


def _main():
    srs = 7296466011
    page = 7
    folder = 'D:/websites/python_learning/amazon_parser/saved_html/'

    saved_file_name = save_amazon_page(srs,page,folder)
    html_doc = getFileContent(saved_file_name)
    soup = BeautifulSoup(html_doc, 'html.parser')
    html_items = soup.find_all("li", class_="s-result-item")

    Base.metadata.create_all(engine)
    session = Session()

    for cur_item in html_items:
        try:
            asin = cur_item['data-asin']
        except:
            continue
        name = cur_item.find("h2").string
        pre_seller = cur_item.find("span", class_="a-size-small a-color-secondary", string="by ")
        seller = pre_seller.next_sibling.string if (pre_seller) else ''

        price_integer = cur_item.find("span", class_="sx-price-whole").string
        price_integer = price_integer.replace(",","")
        price_fractional = cur_item.find("sup", class_="sx-price-fractional").string
        price = int(price_integer) + int(price_fractional)/100

        this_product = session.query(Product).filter(Product.asin == asin, Product.iscurrent == 1).first()
        current_product = None

        if this_product:
            existing_price = this_product.price

            if float(existing_price) != float(price):
                current_product = Product(name, asin, price, seller)
                this_product.iscurrent = 0
        else:
            current_product = Product(name, asin, price, seller)

        if current_product != None:
            print(asin+" : "+str(price))
            session.add(current_product)
            session.commit()

    session.close()


if __name__ == "__main__":
    _main()


# C:\ENV\Scripts\deactivate