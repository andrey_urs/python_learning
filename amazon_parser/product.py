from sqlalchemy import Column, String, Integer
from base import Base
from datetime import datetime


class Product(Base):
    __tablename__ = 'product'

    id=Column(Integer, primary_key=True)
    name=Column(String)
    asin=Column(String)
    price=Column(String)
    seller=Column(String)
    date=Column(String, nullable=False, default=str(datetime.now().strftime(r"%Y-%m-%d %H:%M:%S")))
    iscurrent=Column(Integer, nullable=False, default=1)

    def __init__(self, name, asin, price, seller, \
            date = str(datetime.now().strftime(r"%Y-%m-%d %H:%M:%S")), iscurrent = 1):
        self.name = name
        self.asin = asin
        self.price = price
        self.seller = seller
        self.date = date
        self.iscurrent = iscurrent

