import requests
from random import choice
from bs4 import BeautifulSoup


# http://pgaref.com/blog/python-proxy/
def freeProxy_url_parser(self, web_url):
    curr_proxy_list = []
    content = requests.get(web_url).content
    soup = BeautifulSoup(content, "html.parser")
    table = soup.find("table", attrs={"class": "display fpltable"})
    # The first tr contains the field names.
    headings = [th.get_text() for th in table.find("tr").find_all("th")]
    datasets = []
    for row in table.find_all("tr")[1:]:
        dataset = zip(headings, (td.get_text() for td in row.find_all("td")))
        datasets.append(dataset)
    for dataset in datasets:
        # Check Field[0] for tags and field[1] for values!
        proxy = "http://"
        for field in dataset:
            if field[0] == 'IP Address':
                proxy = proxy+field[1]+':'
            elif field[0] == 'Port':
                proxy = proxy+field[1]
        curr_proxy_list.append(proxy.__str__())
        #print "{0:<10}: {1}".format(field[0], field[1])
    #print "ALL: ", curr_proxy_list
    return curr_proxy_list


# http://edmundmartin.com/random-user-agent-requests-python/
desktop_agents = ['Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
                 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
                 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
                 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0']

def random_headers():
    return {'User-Agent': choice(desktop_agents),'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}


# https://gist.github.com/keithweaver/b0912519d410b7e2ab3c98bf350bcfc2
def getFileContent(pathAndFileName):
    with open(pathAndFileName, 'r') as theFile:
        # Return a list of lines (strings)
        # data = theFile.read().split('\n')
        
        # Return as string without line breaks
        # data = theFile.read().replace('\n', '')
        
        # Return as string
        data = theFile.read()
    return data


def save_amazon_page(srs,page,folder):
    url = f'https://www.amazon.com/s/ref=sr_pg_'+str(page)+'?srs='+str(srs)+'&page='+str(page)
    try:
        file_name = 'parsed_'+str(srs)+'_'+str(page)+'.html'
        file_name = folder + file_name
        html_doc = requests.get(url, headers=random_headers())
        with open(file_name, 'wb') as saved_file:
            saved_file.write(html_doc.content)
        return file_name
    except:
        return None

# def save_amazon_page(srs,page,folder):
#     return 'D:/websites/python_learning/amazon_parser/saved_html/parsed_7296466011_1.html'


if __name__ == "__main__":
    _main()