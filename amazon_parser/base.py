from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker


# engine = create_engine ( 'sqlite:///:memory:' , echo = True )
engine = create_engine ( 'sqlite:///D:/websites/python_learning/amazon_parser/sqlite.db' , echo = False )
Session = sessionmaker(bind=engine)
Base = declarative_base()
